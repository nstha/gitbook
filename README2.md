
## Mark a work order as `Completed`
To mark a work order as `Completed`, navigate to work order details page and click on the FAB to open more options. Then click `Mark as Completed` button.